package com.clearos.clearnav.features.launcher;

import android.util.Log;

import java.util.Calendar;

import com.clearos.clearnav.activity.SplashActivity;
import com.clearos.clearnav.core.events.AppAddEvent;
import com.clearos.clearnav.core.events.AppChangeEvent;
import com.clearos.clearnav.core.events.AppRemoveEvent;
import com.clearos.clearnav.core.events.Event;
import com.clearos.clearnav.core.events.EventRelay;
import com.clearos.clearnav.core.events.ForceReloadEvent;
import com.clearos.clearnav.core.events.ShortcutAddEvent;
import com.clearos.clearnav.core.events.TimeChangedEvent;

public class EventsObserverImpl implements EventRelay.EventsObserver<Event> {

    private static final String TAG = "EventsObserverImpl";

    private SplashActivity splashActivity;

    public EventsObserverImpl(SplashActivity activity) {
        this.splashActivity = activity;
    }

    @Override
    public void accept(Event event) {
        Log.i(TAG, "accept: "+event.getEventType());
        switch (event.getEventType()) {
            case AppAddEvent.TYPE:
                splashActivity.onAppAddEvent((AppAddEvent) event);
                break;
            case AppChangeEvent.TYPE:
                splashActivity.onAppChangeEvent((AppChangeEvent) event);
                break;
            case AppRemoveEvent.TYPE:
                splashActivity.onAppRemoveEvent((AppRemoveEvent) event);
                break;
            case ShortcutAddEvent.TYPE:
                splashActivity.onShortcutAddEvent((ShortcutAddEvent) event);
                break;
            case TimeChangedEvent.TYPE:
                splashActivity.updateAllCalendarIcons(Calendar.getInstance());
                break;
            case ForceReloadEvent.TYPE:
                splashActivity.forceReload();
                break;
        }
    }

    @Override
    public void complete() {
        //BlissLauncher.getApplication(splashActivity).getAppProvider().reload();
    }

    @Override
    public void clear() {
        this.splashActivity = null;
    }
}
