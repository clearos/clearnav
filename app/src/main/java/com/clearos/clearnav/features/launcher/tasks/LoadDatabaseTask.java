package com.clearos.clearnav.features.launcher.tasks;

import android.os.AsyncTask;

import java.util.List;

import com.clearos.clearnav.core.database.LauncherDB;
import com.clearos.clearnav.core.database.model.LauncherItem;
import com.clearos.clearnav.core.migrate.Migration;
import com.clearos.clearnav.features.launcher.AppProvider;

public class LoadDatabaseTask extends AsyncTask<Void, Void, List<LauncherItem>> {

    private AppProvider mAppProvider;

    public LoadDatabaseTask() {
        super();
    }

    public void setAppProvider(AppProvider appProvider) {
        this.mAppProvider = appProvider;
    }

    @Override
    protected List<LauncherItem> doInBackground(Void... voids) {
        Migration.migrateSafely(mAppProvider.getContext());
        return LauncherDB.getDatabase(mAppProvider.getContext()).launcherDao().getAllItems();
    }

    @Override
    protected void onPostExecute(List<LauncherItem> launcherItems) {
        super.onPostExecute(launcherItems);
        if (mAppProvider != null) {
            mAppProvider.loadDatabaseOver(launcherItems);
        }
    }
}
