package com.clearos.clearnav.core.events;

import com.clearos.clearnav.core.database.model.ShortcutItem;

public class ShortcutAddEvent extends Event {
    private ShortcutItem mShortcutItem;

    public static final int TYPE = 603;

    public ShortcutAddEvent(ShortcutItem shortcutItem) {
        super(TYPE);
        this.mShortcutItem = shortcutItem;
    }

    public ShortcutItem getShortcutItem() {
        return mShortcutItem;
    }
}
