package com.clearos.clearnav.core.events;

public class TimeChangedEvent extends Event {

    public static final int TYPE = 701;

    public TimeChangedEvent() {
        super(TYPE);
    }
}
