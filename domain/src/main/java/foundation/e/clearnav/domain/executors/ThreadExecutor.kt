package com.clearos.clearnav.domain.executors

import java.util.concurrent.Executor

interface ThreadExecutor : Executor